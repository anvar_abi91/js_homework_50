import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent {
  @Input() title = '';
  @Input() year = '';
  icon = 'https://upload.wikimedia.org/wikipedia/en/0/0d/Avengers_Endgame_poster.jpg';
  iconTwo = 'https://ae04.alicdn.com/kf/HTB1.9.JOVXXXXbUXpXXq6xXFXXXr/-.jpg';
}


